// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "TeamsManager.h"
#include "InGameHudHandler.h"
#include "Poll.h"


// PUBLIC METHODS

// Sets default values for this actor's properties
APoll::APoll()
{
	bIsActive = false;
	bNeedsTarget = true;
	PrimaryActorTick.bCanEverTick = false;
	DefaultTarget = TEXT("NULL");

	CurrentRescheduleTime = RescheduleTime;
}


bool APoll::Init_Validate(int32 nId, AProjectCharacter* nOwner)
{
	return true;
}

// Sets default values for this actor's properties
void APoll::Init_Implementation(int32 nId, AProjectCharacter* nOwner)
{
	UE_LOG(LogTemp, Error, TEXT("multicast Poll Init"));

	Owner = nOwner;
	Id = nId;

	generatePossibleTargets();

	UInGameHudHandler::instance->OnCreatePoll(nId, nOwner, PollType, PollInteractionType);
}



// Process the message received looking for the keyword and targets, and registering the vote
void APoll::ProcessMessage(FString Message)
{
	if (bIsActive)
	{
		FString Voter, Keyword, Target;
		if (HasKeyword(Message, Voter, Keyword, Target))
		{
			ComputeVote(Voter, Keyword, Target);
		}
	}
}


// Starts a poll by reseting the configurations
void APoll::StartPoll(TSet<FString> Keywords, TSet<FString> Targets, FString nDefaultTarget)
{
	PrimaryActorTick.bCanEverTick = true;
	PossibleKeywords = Keywords;

	PossibleTargets = Targets;
	PossibleTargets.Add(nDefaultTarget);
	this->DefaultTarget = nDefaultTarget;

	ViewersVotes.Empty();
	PairsVotes.Empty();

	//Initialize PairsVotes with all the possible pairs, and with value 0
	for (auto& k : PossibleKeywords)
	{
		PairsVotes.Add(k);
		for (auto& t : PossibleTargets)
		{
			PairsVotes[k].Add(t, 0);
		}
	}

	bIsActive = true;
}


// Starts a poll without target by reseting the configurations
void APoll::StartPoll(TSet<FString> Keywords)
{
	StartPoll(Keywords, TSet<FString>(), TEXT("NULL"));
}

// Gets the poll's preliminary results, sorted from most votes to less votes
TArray<TPair<int32, TPair<FString, FString> > > APoll::GetPollResults()
{
	//If votes are unique, must convert from ViewersVotes Structure to PairsVote
	if (!bCanMultipleVotes) 
	{
		// Resets the PairsVotes (could be changed during last GetPollResults())
		for (auto& k : PossibleKeywords)
		{
			for (auto& t : PossibleTargets)
			{
				PairsVotes[k][t] = 0;
			}
		}

		// For every vote, the pair count is increased
		for (auto& Vote : ViewersVotes)
		{
			PairsVotes[Vote.Value.Key][Vote.Value.Value]++;
		}
	}

	// With PairsVotes ready, converts to the output format
	TArray<TPair<int32, TPair<FString, FString> > > Results;

	for (auto& k : PairsVotes)
	{
		for (auto& t : k.Value)
		{
			TPair<int32, TPair<FString, FString> > pair;
			pair.Key = t.Value;
			pair.Value.Key = k.Key;
			pair.Value.Value = t.Key;
			Results.Add(pair);
		}
	}

	// Sort by votes, in descending order
	Results.Sort([](const TPair<int32, TPair<FString, FString> >& A, const TPair<int32, TPair<FString, FString> >& B) {
		return A.Key > B.Key;
	});

	return Results;
}


// Stops the chosen poll and delete its information
void APoll::StopPoll()
{
	bIsActive = false;

	PossibleKeywords.Empty();
	PossibleTargets.Empty();
	ViewersVotes.Empty();
	PairsVotes.Empty();
}


// Gets the available keywords for this poll
TArray<FString> APoll::GetKeywords()
{
	return PossibleKeywords.Array();
}


// Gets the available targets for this poll
TArray<FString> APoll::GetTargets()
{
	return PossibleTargets.Array();
}


// Checks the message if the sender has chosen a keyword, updating the voter, keyword and target
bool APoll::HasKeyword(FString Message, FString &OutVoter, FString &OutKeyword, FString &OutTarget)
{
	Message = Message.ToLower();
	// The next statement removes 2 invisible characters at the end of
	// the message received through the network
	Message.RemoveAt(Message.Len() - 2, 2);

	// Split the message in the following characters
	TArray<FString> Tokens;
	const TCHAR* Delims[] = { TEXT(" "), TEXT(":"), TEXT("!") };
	int32 NumberOfElements = Message.ParseIntoArray(Tokens, Delims, 3);

	// The format is
	// :user!user@user.tmi.twitch.tv privmsg #user :message
	// therefore, after the parsing, in the Tokens array:
	// 0   | user
	// 1   | user@user.tmi.twitch.tv
	// 2   | privmsg
	// 3   | #user
	// 4.. | parsed message
	if (NumberOfElements >= 5)
	{
		OutVoter = Tokens[0];
		if (Tokens[2].Equals(TEXT("privmsg")))
		{
			// Search the message tokens in the valid Keywords
			// If more then one keyword appears, THE FIRST one is considered
			// The target MUST follow the keyword, or it will be assumed to be "self"
			for (int32 i = 4; i < NumberOfElements; i++)
			{
				if (PossibleKeywords.Contains(Tokens[i]))
				{
					OutKeyword = Tokens[i];
					if (i == NumberOfElements - 1 || !PossibleTargets.Contains(Tokens[i + 1]))
					{
						OutTarget = DefaultTarget;
					}
					else
					{
						OutTarget = Tokens[i + 1];
					}

					return true;
				}
			}
		}
	}

	return false;
}


// Register the Voter's vote, with the keyword and target specified
void APoll::ComputeVote(FString Voter, FString Keyword, FString Target)
{
	if (bCanMultipleVotes) // If votes aren't unique, simply use PairsVotes Structure
	{
		PairsVotes[Keyword][Target]++;
	}
	else //else, must fill the ViewersVotes Strucure instead
	{
		if (ViewersVotes.Contains(Voter))
		{
			ViewersVotes[Voter].Key = Keyword;
			ViewersVotes[Voter].Value = Target;
		}
		else
		{
			TPair<FString, FString> Pair;
			Pair.Key = Keyword;
			Pair.Value = Target;
			ViewersVotes.Add(Voter, Pair);
		}
	}
}


FString APoll::getTargetableKeyword (AProjectCharacter * Target)
{
	return Target->PlayerName;
}


void APoll::generatePossibleTargets ()
{
	switch(TargetSelection)
	{
		case EPollTargetSelection::PTS_None:
			DefaultTarget = TEXT("NULL");
			bNeedsTarget = false;
			break;

		case EPollTargetSelection::PTS_AllRandom:
			DefaultTarget = TEXT("NULL");
			bNeedsTarget = false;

			if (ATeamsManager::instance != nullptr) {
				TArray<FString> keys;
				ATeamsManager::instance->Players.GenerateKeyArray(keys);

				PossibleTargets = TSet<FString>(keys);
			}

			break;

		case EPollTargetSelection::PTS_TeamRandom:
			DefaultTarget = TEXT("NULL");
			bNeedsTarget = false;

			if (ATeamsManager::instance != nullptr)
				PossibleTargets = TSet<FString>(ATeamsManager::instance->GetAllPlayersFromTeam(Owner->Team));

			break;

		case EPollTargetSelection::PTS_Self:
			bNeedsTarget = false;
			if (Owner != nullptr)
				DefaultTarget = getTargetableKeyword(Owner);

			break;

		case EPollTargetSelection::PTS_All:
			if (ATeamsManager::instance != nullptr) {
				TArray<FString> keys;
				ATeamsManager::instance->Players.GenerateKeyArray(keys);

				PossibleTargets = TSet<FString>(keys);
			}

			break;

		case EPollTargetSelection::PTS_Team:
			if (ATeamsManager::instance != nullptr)
				PossibleTargets = TSet<FString>(ATeamsManager::instance->GetAllPlayersFromTeam(Owner->Team));
			break;
	}
}