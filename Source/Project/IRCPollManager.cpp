// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include <string>
#include "IRCPollManager.h"

/* ---------------------- PUBLIC METHODS ------------------------------*/

// Sets default values
AIRCPollManager::AIRCPollManager()
{
	// Set this actor to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;
}

// Sets default values
AIRCPollManager::AIRCPollManager(FString UserNick, FString AuthToken, FString UserName, FString UserTargetName)
{
	// Set this actor to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;

	SetPlayer(UserNick, AuthToken, UserName, UserTargetName);
}

// Called when the game starts or when spawned
void AIRCPollManager::BeginPlay()
{
	Super::BeginPlay();

	// Big Number so that from start can answer these requests
	TimeElapsedKeywordsRequest = 100.0f;
	TimeElapsedStreamsRequest = 100.0f;

}

// Called every frame
void AIRCPollManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Update the timers
	TimeElapsedKeywordsRequest += DeltaTime;
	TimeElapsedStreamsRequest += DeltaTime;

	// The received network message
	TArray<uint8> ReceivedData;
	// The size of the message
	uint32 Size;
	// The chosen keyword in the vote
	FString Keyword;
	// The voter
	FString Voter;
	// The target of the vote
	FString Target;

	// Only if the TCP connection is on
	if (bIsConnected)
	{
		// While there are messages to process
		while (Socket->HasPendingData(Size))
		{
			// Receive and convert the received message to FString
			ReceivedData.SetNumUninitialized(FMath::Min(Size, 65507u));
			int32 Read = 0;
			Socket->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read);
			std::string cstr(reinterpret_cast<const char*>(ReceivedData.GetData()), ReceivedData.Num());
			FString Message(cstr.c_str());

			// Deal with PINGs, requests, and at last, with polls
			if (IsPingMessage(Message))
			{
				HasSentTCPMessage(TEXT("PONG :tmi.twitch.tv"));
			}
			else if (IsKeywordsRequest(Message))
			{
				AnswerKeywordsRequest();
			}
			else if (IsStreamsRequest(Message))
			{
				AnswerStreamsRequest();
			}
			else
			{
				// For each ongoing poll, try to register a vote 
				// (to deal with weather and arena polls all at the same time)
				for (auto& poll : Polls)
				{
					poll.Value->ProcessMessage(Message);
				}
			}
		}
	}
}

// Sets player information
void AIRCPollManager::SetPlayer(FString nUserNick, FString nAuthToken, FString nUserName, FString nUserTargetName)
{
	this->UserNick = nUserNick;
	this->AuthToken = nAuthToken;
	this->UserName = nUserName;
	this->UserTargetName = nUserTargetName;
}

// Try to connect to the player's twitch irc channel
bool AIRCPollManager::TryConnect()
{
	// Create the TCP Socket
	Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(FName(NAME_Stream), TEXT("IRCSocket"), false);

	if (Socket == NULL)
	{
		bIsConnected = false;
		return false;
	}

	// Set socket buffer size properties
	int32 SocketSize = 2 * 1024 * 1024;
	Socket->SetSendBufferSize(SocketSize, SocketSize);
	SocketSize = 2 * 1024 * 1024;
	Socket->SetReceiveBufferSize(SocketSize, SocketSize);

	// Create an internet address, get the ip by the host name and set the IRC default port (6667)
	TSharedRef<FInternetAddr> Addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->GetHostByName("irc.twitch.tv", *Addr);
	Addr->SetPort(6667);

	// Estabilishes the TCP connection
	bIsConnected = Socket->Connect(*Addr);

	if (bIsConnected)
	{
		bool b = true;
		FString Message;

		// Executes the IRC handshake protocol!
		//b = b && HasSentTCPMessage(TEXT("CAP LS"));
		Message = FString::Printf(TEXT("PASS oauth:%s"), *AuthToken);
		b = b && HasSentTCPMessage(Message);
		Message = FString::Printf(TEXT("NICK %s"), *UserNick);
		b = b && HasSentTCPMessage(Message);
		Message = FString::Printf(TEXT("USER %s %s irc.twitch.tv :%s"), *UserNick, *UserNick, *UserName);
		b = b && HasSentTCPMessage(Message);
		//b = b && HasSentTCPMessage(TEXT("CAP END"));
		Message = FString::Printf(TEXT("JOIN #%s"), *UserNick);
		b = b && HasSentTCPMessage(Message);

		if (b)
		{
			HasSentMessage(TEXT("Welcome to Twitch Arena!"));
		}
		else
		{
			bIsConnected = false;
			Socket->Close();
		}
	}

	return bIsConnected;
}


void AIRCPollManager::RegisterPoll(APoll* Poll, EPollType PollType)
{
	AllPolls.Add(PollType, Poll);
}


// Try to send the message to twitch IRC with the current account to the current channel
bool AIRCPollManager::HasSentMessage(FString Message)
{
	// Sets the IRC command to send a message
	FString SentMessage = FString::Printf(TEXT("PRIVMSG #%s :[TAB] %s"), *UserNick, *Message);
	return HasSentTCPMessage(SentMessage);
}

// Start to register votes for a new poll, with the available keywords and targets
void AIRCPollManager::StartPoll(FString PollName, TSet<FString> AvailableKeywords, TSet<FString> AvailableTargets, 
	int32 RelevanceLevel, bool bCanMultipleVotes)
{
	
	APoll* P = (APoll*) NewObject<APoll>(this, APoll::StaticClass());
	// P->Init(RelevanceLevel, bCanMultipleVotes);
	P->StartPoll(AvailableKeywords, AvailableTargets, UserTargetName);
	Polls.Add(PollName, P);
	HasSentMessage("New Poll Started!");
}

// Start to register votes for a new poll, with the available keywords
void AIRCPollManager::StartPoll(FString PollName, TSet<FString> AvailableKeywords,
	int32 RelevanceLevel, bool bCanMultipleVotes)
{
	APoll* P = (APoll*)NewObject<APoll>(this, APoll::StaticClass());
	// P->Init(RelevanceLevel, bCanMultipleVotes);
	P->StartPoll(AvailableKeywords);
	Polls.Add(PollName, P);
	HasSentMessage("New Poll Started!");
}

// Gets the poll's preliminary results sorted from most voted to least voted
TArray<TPair<int32, TPair<FString, FString> > > AIRCPollManager::GetPollResults(FString PollName)
{
	if (Polls.Contains(PollName))
	{
		return Polls[PollName]->GetPollResults();
	}
	return TArray<TPair<int32, TPair<FString, FString> > >();
}

//	Stops the chosen poll and delete its information
void AIRCPollManager::StopPoll(FString PollName) {
	if (Polls.Contains(PollName))
	{
		Polls[PollName]->StopPoll();
		//Polls[PollName]->BeginDestroy();
		Polls.Remove(PollName);
	}
}

// Set the list of streams of other players in the same match
void AIRCPollManager::SetStreams(TArray<FString> nStreams)
{
	this->Streams = nStreams;
}

/* ---------------------- PRIVATE METHODS ------------------------------*/


// Try to send the serialized message to twitch IRC with the current account
// NOTE: NOT privmsg to a channel, but the TCP message
bool AIRCPollManager::HasSentTCPMessage(FString Message)
{
	// Adds end of line to delimit the message
	Message = TEXT("\n") + Message + TEXT("\n");

	TCHAR *SerializedChar = Message.GetCharArray().GetData();
	int32 Size = FCString::Strlen(SerializedChar);
	int32 Sent = 0;

	return Socket->Send((uint8*)TCHAR_TO_UTF8(SerializedChar), Size, Sent);
}

// Checks if the received message is a ping
bool AIRCPollManager::IsPingMessage(FString Message)
{
	// Split the message in the following characters
	TArray<FString> Tokens;
	const TCHAR* Delims[] = { TEXT(" "), TEXT(":"), TEXT("!") };
	int32 NumberOfElements = Message.ParseIntoArray(Tokens, Delims, 3);

	if (NumberOfElements > 0 && Tokens[0].Equals(TEXT("PING")))
	{
		return true;
	}

	return false;
}

// Checks if the received message contains a !keywords request
bool AIRCPollManager::IsKeywordsRequest(FString Message)
{
	Message = Message.ToLower();
	// The next statement removes 2 invisible characters at the end of
	// the message received through the network
	Message.RemoveAt(Message.Len() - 2, 2);

	// Split the message in the following characters
	TArray<FString> Tokens;
	const TCHAR* Delims[] = { TEXT(" "), TEXT(":") };
	int32 NumberOfElements = Message.ParseIntoArray(Tokens, Delims, 2);

	// The format is
	// :user!user@user.tmi.twitch.tv privmsg #user :message
	// therefore, after the parsing, in the Tokens array:
	// 0   | user
	// 1   | user@user.tmi.twitch.tv
	// 2   | privmsg
	// 3   | #user
	// 4.. | parsed message
	if (NumberOfElements >= 4 && Tokens[1].Equals(TEXT("privmsg")))
	{
		for (int i = 3; i < NumberOfElements; i++)
		{
			if (Tokens[i].Equals(TEXT("!keywords")))
			{
				return true;
			}
		}
	}
	return false;
}

// Checks if the received message is contains a !strams request
bool AIRCPollManager::IsStreamsRequest(FString Message)
{
	Message = Message.ToLower();
	// The next statement removes 2 invisible characters at the end of
	// the message received through the network
	Message.RemoveAt(Message.Len() - 2, 2);

	// Split the message in the following characters
	TArray<FString> Tokens;
	const TCHAR* Delims[] = { TEXT(" "), TEXT(":") };
	int32 NumberOfElements = Message.ParseIntoArray(Tokens, Delims, 2);

	// The format is
	// :user!user@user.tmi.twitch.tv privmsg #user :message
	// therefore, after the parsing, in the Tokens array:
	// 0   | user
	// 1   | user@user.tmi.twitch.tv
	// 2   | privmsg
	// 3   | #user
	// 4.. | parsed message
	if (NumberOfElements >= 4 && Tokens[1].Equals(TEXT("privmsg")))
	{
		for (int i = 3; i < NumberOfElements; i++)
		{
			if (Tokens[i].Equals(TEXT("!streams")))
			{
				return true;
			}
		}
	}

	return false;
}

//	Answer the request for the valid keywords for the more relevant polls, in the IRC chat
void AIRCPollManager::AnswerKeywordsRequest()
{
	if (TimeElapsedKeywordsRequest >= 15.0f)
	{
		TimeElapsedKeywordsRequest = 0.0f;
		int32 currentRelevance = -1; // less relevance possible
		TSet<FString> Keywords; // Use of sets to avoid multiple keywords

		for (auto& Poll : Polls)
		{
			if (Poll.Value->Priority < currentRelevance)
			{
				continue;
			}
			if (Poll.Value->Priority > currentRelevance) 
			{
				currentRelevance = Poll.Value->Priority;
				Keywords.Empty();
			}

			Keywords.Append(Poll.Value->GetKeywords());
		}

		FString Keys = TEXT("Keywords: ");
		for (auto& k : Keywords)
		{
			Keys.Append(k);
			Keys.Append(TEXT(" "));
		}

		HasSentMessage(Keys);
	}
}

// Answer the request for the valid streamers, in the IRC chat
void AIRCPollManager::AnswerStreamsRequest()
{
	if (TimeElapsedStreamsRequest >= 15.0f)
	{
		TimeElapsedStreamsRequest = 0.0f;

		FString Message = TEXT("Streams: ");
		for (auto& Streamer : Streams)
		{
			Message.Append(TEXT("twitch.tv/"));
			Message.Append(Streamer);
			Message.Append(TEXT(" "));
		}

		HasSentMessage(Message);
	}
}