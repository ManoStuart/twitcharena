// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Project.h"
#include "EngineUtils.h"
#include "UnrealNetwork.h"
#include "DataSingleton.h"
#include "TeamsManager.h"
#include "PollManagementSystem.h"
#include "ProjectCharacter.h"


template<typename TEnum>
static FORCEINLINE FString GetEnumValueToString(const FString& Name, TEnum Value)
{
	const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
	if (!enumPtr)
	{
		return FString("Invalid");
	}

	return enumPtr->GetEnumName((int32)Value);
}

//////////////////////////////////////////////////////////////////////////
// AProjectCharacter

AProjectCharacter::AProjectCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer.SetDefaultSubobjectClass<UMyCharacterMovement>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(30.f, 85.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = true;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 500.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Prevent Hanging at the edge of drops
	GetCharacterMovement()->PerchRadiusThreshold = 25;
	GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;


	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->AttachToComponent(CameraBoom, FAttachmentTransformRules::KeepRelativeTransform);
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)


	ServerNextSection = -2;

	// Health setup
	Health = 100;
}

//////////////////////////////////////////////////////////////////////////
// Default

void AProjectCharacter::BeginPlay()
{
	Super::BeginPlay();

	Mesh = Cast<USkeletalMeshComponent>(this->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	Animation = Cast<UAnimInstance>(Mesh->GetAnimInstance());

	if (HUD)
		UE_LOG(LogTemp, Warning, TEXT("HUD Initiated"));
	if (HudHandler)
		UE_LOG(LogTemp, Warning, TEXT("HudHandler Initiated"));

	if (HasAuthority()) {
		if (ATeamsManager::instance != nullptr)
	  	ATeamsManager::instance->AssignTeam(this);

	   FTimerHandle UnusedHandle;
		 GetWorldTimerManager().SetTimer(
		     UnusedHandle, this, &AProjectCharacter::AfterNetworkInit, 4.0f, false);
	}
}

void AProjectCharacter::AfterNetworkInit()
{
	UE_LOG(LogTemp, Error, TEXT("AProjectCharacter AfterNetworkInit"));
	if (APollManagementSystem::instance != nullptr)
  	APollManagementSystem::instance->AddPlayer(TEXT("manostuart"), TEXT("054pwwl2kz38srsterq0bbd08bgsss"), TEXT("Mano Stuart"), this);
}

void AProjectCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AProjectCharacter, ActionQueue);
	DOREPLIFETIME(AProjectCharacter, Health);

	// TODO: Properly assign teams.
	DOREPLIFETIME(AProjectCharacter, Team);
	DOREPLIFETIME(AProjectCharacter, bDodging);
}

void AProjectCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (this->IsLocallyControlled()) {
		this->UpdateBestInteraction();
	}


	UMyCharacterMovement* MoveComp = Cast<UMyCharacterMovement>(GetCharacterMovement());

	if (bDodging && !MoveComp->HasPendingLaunch())
		bDodging = false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AProjectCharacter::SetupPlayerInputComponent(class UInputComponent* nInputComponent)
{
	PrimaryActorTick.bCanEverTick = true;

	// Set up gameplay key bindings
	check(nInputComponent);
	nInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	nInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	nInputComponent->BindAxis("MoveForward", this, &AProjectCharacter::MoveForward);
	nInputComponent->BindAxis("MoveRight", this, &AProjectCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	nInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	nInputComponent->BindAxis("TurnRate", this, &AProjectCharacter::TurnAtRate);
	nInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	nInputComponent->BindAxis("LookUpRate", this, &AProjectCharacter::LookUpAtRate);

	// handle action inputs
	nInputComponent->BindAction("Action", IE_Pressed, this, &AProjectCharacter::QueueAction);
	nInputComponent->BindAction("Action", IE_Released, this, &AProjectCharacter::QueueReleasedAction);
	nInputComponent->BindAction("SubAction", IE_Pressed, this, &AProjectCharacter::QueueSubAction);
	nInputComponent->BindAction("SubAction", IE_Released, this, &AProjectCharacter::QueueReleasedAction);
	
	// handle equip inputs
	nInputComponent->BindAction("EquipDefault", IE_Pressed, this, &AProjectCharacter::EquipItem1);
	nInputComponent->BindAction("EquipWeapon", IE_Pressed, this, &AProjectCharacter::EquipItem2);
	nInputComponent->BindAction("EquipUsable", IE_Pressed, this, &AProjectCharacter::EquipItem3);

	// handle interaction inputs
	nInputComponent->BindAction("Interaction", IE_Pressed, this, &AProjectCharacter::Interact);

	// handle roll
	nInputComponent->BindAction("Roll", IE_Released, this, &AProjectCharacter::Dogde);
}

bool AProjectCharacter::CanJumpInternal_Implementation() const
{
	bool bCanJump = Super::CanJumpInternal_Implementation();

	UMyCharacterMovement* MyMovementComp = Cast<UMyCharacterMovement>(GetCharacterMovement());
	if (!bCanJump && MyMovementComp)
	{
		bCanJump = MyMovementComp->CanJump();
	}

	return bCanJump;
}

void AProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AProjectCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AProjectCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AProjectCharacter::Dogde()
{
	UMyCharacterMovement* MoveComp = Cast<UMyCharacterMovement>(GetCharacterMovement());
	if (MoveComp)
	{
		bDodging = MoveComp->DoDodge();
	}
}

void AProjectCharacter::QueueAction()
{
	ServerQueueAction(EActionQueue::AQ_Basic);
}

void AProjectCharacter::QueueSubAction()
{
	ServerQueueAction(EActionQueue::AQ_Power);
}


void AProjectCharacter::QueueReleasedAction()
{
	ServerQueueAction(EActionQueue::AQ_Released);
}

void AProjectCharacter::EquipItem1()
{
	if (OwnedItems.Find(EItemType::IT_Default) == nullptr) return;

	ServerQueueAction(EActionQueue::AQ_Item1);
}

void AProjectCharacter::EquipItem2()
{
	if (OwnedItems.Find(EItemType::IT_Weapon) == nullptr) return;

	ServerQueueAction(EActionQueue::AQ_Item2);
}

void AProjectCharacter::EquipItem3()
{
	if (OwnedItems.Find(EItemType::IT_Usable) == nullptr) return;

	ServerQueueAction(EActionQueue::AQ_Item3);
}

//////////////////////////////////////////////////////////////////////////
// Movement Control

void AProjectCharacter::SetDisableAllMovement(bool move)
{
	DisableAllMovement = move;

	UMyCharacterMovement* MoveComp = Cast<UMyCharacterMovement>(GetCharacterMovement());
	if (MoveComp)
	{
		bUseControllerRotationYaw = !DisableAllMovement;
		MoveComp->bDisableAllMovement = DisableAllMovement;

		if (DisableAllMovement) {
			MoveComp->DisableMovement();
		}
		else {
			MoveComp->SetMovementMode(EMovementMode::MOVE_Walking);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Animation Utils

void AProjectCharacter::EquipItemType(EItemType type) {
	if (type == EItemType::IT_Default) AProjectCharacter::EquipItem1();
	else if (type == EItemType::IT_Weapon) AProjectCharacter::EquipItem2();
	else if (type == EItemType::IT_Usable) AProjectCharacter::EquipItem3();
}

void AProjectCharacter::StopActionCombo()
{
	//UE_LOG(LogTemp, Warning, TEXT("Stop Combo!"));
	InAction = false;
	CurSection = -1;
	CurQueue = 0;
	CurCombo = 0;
	ActionQueue.Empty();
	SetDisableAllMovement(false);
}

bool AProjectCharacter::PlayAnimation(int32 target)
{
	//UE_LOG(LogTemp, Warning, TEXT("Play Animation!"));
	CurSection = target;
	bool canMove;
	UAnimSequenceBase* anim = EquippedItem->GetAnimAsset(CurSection, canMove);

	UMyCharacterMovement* MoveComp = Cast<UMyCharacterMovement>(GetCharacterMovement());

	// Cant make standing animations in air.
	if (!canMove && MoveComp->IsFalling()) {
		StopActionCombo();
		return false;
	}

	// Set Next Combo Move
	else if (anim != nullptr) {
		SetDisableAllMovement(!canMove);

		FName slot = FName(*FString(TEXT("Action")));
		if (!canMove)
			slot = FName(*FString(TEXT("Root")));

		Animation->PlaySlotAnimationAsDynamicMontage(anim, slot);
		return true;
	}

	// Invalid Combo progression
	else {
		StopActionCombo();
		return false;
	}
}

bool AProjectCharacter::ChangeEquippedItem(EItemType slot)
{
	AItem** target = OwnedItems.Find(slot);

	// Already equipped or dont own the targeted item.
	if (target == nullptr || EquippedItem == *target || slot == EItemType::IT_None) return false;

	EquippedItem->SetActorHiddenInGame(true);

	// Peform scheduled unequip
	if (bScheduledUnequip)
		PeformUnequip(EquippedItem);

	EquippedItem = *target;
	CurSection = 0;

	EquippedItem->SetActorHiddenInGame(false);

	if (EquippedItem->EquipAnim == nullptr)
		return false;

	Animation->PlaySlotAnimationAsDynamicMontage(EquippedItem->EquipAnim, FName(*FString(TEXT("Action"))));
	EquippedItem->Equip();

	return true;
}

//////////////////////////////////////////////////////////////////////////
// Animation Networking

bool AProjectCharacter::ServerQueueAction_Validate(EActionQueue action)
{
  return true;
}

void AProjectCharacter::ServerQueueAction_Implementation(EActionQueue action) {
	//UE_LOG(LogTemp, Warning, TEXT("Server Queue!"));

	// Change EquippedItem
	if (UDataSingleton::IsChangeItem(action)) {
		EItemType type = UDataSingleton::ItemActionToType(action);
		AItem** target = OwnedItems.Find(type);

		// Already equipped or dont own the targeted item.
		if (target == nullptr || EquippedItem == *target || type == EItemType::IT_None) return;

		ActionQueue.SetNum(CurQueue, true);
		ActionQueue.Add(action);
	}

	else {
		// Check if releasing makes sense
		if (action == EActionQueue::AQ_Released) {
			if (ActionQueue.Num() == 0) return;

			EActionQueue last = ActionQueue.Last();

			if (last != EActionQueue::AQ_Basic && last != EActionQueue::AQ_Power)
				return;

			ActionQueue.Add(action);
		}

		// Store max 1 actions ahead.
		else if (ActionQueue.Num() > CurQueue) {
			ActionQueue[CurQueue] = action;
			ActionQueue.SetNum(CurQueue + 1);
		}
		else
			ActionQueue.Add(action);
	}


	if (!InAction) {
		InAction = true;
		MulticastStartAction(action);
	}
}

bool AProjectCharacter::MulticastStartAction_Validate(EActionQueue action)
{
	return true;
}

void AProjectCharacter::MulticastStartAction_Implementation(EActionQueue action) {
	//UE_LOG(LogTemp, Warning, TEXT("Multicast Start!"));

	if (EquippedItem == NULL) {
		UE_LOG(LogTemp, Warning, TEXT("NULL Item!"));
		return;
	}

	InAction = true;
	CurQueue = 1;
	CurCombo = 1;

	// Set Animation
	if (UDataSingleton::IsChangeItem(action))
		ChangeEquippedItem(UDataSingleton::ItemActionToType(action));
	else
		PlayAnimation(EquippedItem->GetNextAnim(0, action));
}

bool AProjectCharacter::ValidateComboAction_Validate(int32 QueuePos, int32 ComboPos, int32 TargetAnimation, EItemType type)
{
	return true;
}

void AProjectCharacter::ValidateComboAction_Implementation(int32 QueuePos, int32 ComboPos, int32 TargetAnimation, EItemType type) {
	// Client Dind't run NextCombo yet
	if (CurCombo <= ComboPos - 1) {
		ServerNextSection = TargetAnimation;
		CurQueue = QueuePos;
		ServerNextWeapon = type;
		CurCombo = ComboPos;
	}
	// Client Ahead of server
	else if (CurCombo > ComboPos + 1) {
		//UE_LOG(LogTemp, Error, TEXT("Client Animation Ahead of server!"));

		ServerNextSection = TargetAnimation;
		CurQueue = QueuePos;
		ServerNextWeapon = type;
		CurCombo = ComboPos;
	}
	else
		ServerNextSection = -2;
}


//////////////////////////////////////////////////////////////////////////
// Animations

void AProjectCharacter::NextActionQueued() {
	//check(ActionQueue.Num() > 0);
	//UE_LOG(LogTemp, Warning, TEXT("Next Montage!"));

	if (EquippedItem == NULL) {
		UE_LOG(LogTemp, Warning, TEXT("NULL Item!"));
		return;
	}

	bool inAnim = false;

	// Stored server combo progression
	if (ServerNextSection != -2) {
		if (EquippedItem->ItemType != ServerNextWeapon)
			inAnim = ChangeEquippedItem(ServerNextWeapon);
		else
			inAnim = PlayAnimation(ServerNextSection);

		ServerNextSection = -2;
	}

	// Failsafe
	else if (ActionQueue.Num() == 0)
		StopActionCombo();

	else if (CurQueue >= ActionQueue.Num()) {
		EActionQueue action = ActionQueue.Last();

		// Holding Move Action
		if (UDataSingleton::IsMoveAction(action)) {
			// Fix so next moves get aligned properly
			CurQueue--;

			inAnim = PlayAnimation(EquippedItem->GetNextAnim(CurSection, action));
		}

		// Stop Combo
		else
			StopActionCombo();
	}

	// Next Combo
	else {
		EActionQueue action = ActionQueue[CurQueue];

		// Ignore Release actions
		if (action == EActionQueue::AQ_Released) {
			CurQueue++;

			// Stop Combo
			if (CurQueue >= ActionQueue.Num())
				StopActionCombo();

			else
				action = ActionQueue[CurQueue];
		}


		if (UDataSingleton::IsChangeItem(action))
			inAnim = ChangeEquippedItem(UDataSingleton::ItemActionToType(action));
		else
			inAnim = PlayAnimation(EquippedItem->GetNextAnim(CurSection, action));
	}


	// Send to clients, server combo progression
	if (HasAuthority())
		ValidateComboAction(CurQueue, CurCombo, CurSection, EquippedItem->ItemType);

	if (inAnim) {
		CurQueue++;
		CurCombo++;
	}
}

//////////////////////////////////////////////////////////////////////////
// Equip

void AProjectCharacter::Equip(AItem* item, bool hidden, bool equip)
{
	//UE_LOG(LogTemp, Error, TEXT("Equip!"));
	AItem** currentItem = OwnedItems.Find(item->ItemType);

	if (currentItem != nullptr)
		PeformUnequip(*currentItem);
	
	AttachItem(item, hidden);

	OwnedItems.Add(item->ItemType, item);

	if (equip)
		EquipItemType(item->ItemType);
}

void AProjectCharacter::Unequip(AItem* current)
{
	if (current == nullptr)
		current = EquippedItem;

	if (current->ItemType == EItemType::IT_Default) return;

	EquipItemType(EItemType::IT_Default);

	OwnedItems.Remove(current->ItemType);
	bScheduledUnequip = true;
	current->Unequip();
}

void AProjectCharacter::PeformUnequip(AItem* current)
{
	if (current == nullptr)
		current = EquippedItem;

	if (current->ItemType == EItemType::IT_Default) return;

	// Obliterate
	if (current->ShouldDestroy())
		current->Destroy();

	// Spawn in game
	else {
		AItemPhysical::Create(current, this, false);
	}

	bScheduledUnequip = false;
}

//////////////////////////////////////////////////////////////////////////
// Interaction Networking

bool AProjectCharacter::ServerPickUpItem_Validate(AItemPhysical* physical)
{
	return true;
}

void AProjectCharacter::ServerPickUpItem_Implementation(AItemPhysical* physical)
{
	//UE_LOG(LogTemp, Error, TEXT("Server"));
	if (physical == nullptr || physical->dirty) return;
	// Check distance

	physical->dirty = true;

	MulticastPickUpItem(physical);
}


bool AProjectCharacter::MulticastPickUpItem_Validate(AItemPhysical* physical)
{
	return true;
}

void AProjectCharacter::MulticastPickUpItem_Implementation(AItemPhysical* physical)
{
	//UE_LOG(LogTemp, Error, TEXT("multicast"));
	if (physical == nullptr) return;

	Equip(physical->item, false, HasAuthority());
	physical->Destroy();
}


//////////////////////////////////////////////////////////////////////////
// Interactions

void AProjectCharacter::Interact()
{
	if (BestInteractable == nullptr) return;

	ServerPickUpItem(BestInteractable);
}

void AProjectCharacter::RegisterInteractable(AItemPhysical* item)
{
	Interactables.AddUnique(item);
}

void AProjectCharacter::UnregisterInteractable(AItemPhysical* item)
{
	Interactables.Remove(item);
}


//////////////////////////////////////////////////////////////////////////
// Getters

bool AProjectCharacter::HasItemAt(EItemType type)
{
	AItem** target = OwnedItems.Find(type);
	return target == nullptr ? false : true;
}

AItem* AProjectCharacter::GetItemAt(EItemType type)
{
	AItem** target = OwnedItems.Find(type);
	return target == nullptr ? nullptr : *target;
}



//////////////////////////////////////////////////////////////////////////
// Health

void AProjectCharacter::OnTakeAnyDamage(float damage)
{
	// Play Hitted Animation
	// Consider changing slot for hitted animation
	if (!InAction)
		Animation->PlaySlotAnimationAsDynamicMontage(HitedAnim, FName(*FString(TEXT("Action"))));

	// Reduce Health
	Health -= damage;

	// Check Death
	if (Health <= 0)
		UE_LOG(LogTemp, Error, TEXT("U DEAD MAN"));
}