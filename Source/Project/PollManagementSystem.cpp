// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "PollManagementSystem.h"

APollManagementSystem* APollManagementSystem::instance;

// Sets default values
APollManagementSystem::APollManagementSystem()
{
  // Creating singleton
  instance = this;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentPoll = nullptr;
	currentPollDelay = 5;

  if (WeatherPollClass) {
    WeatherPoll = (APoll*) GetWorld()->SpawnActor(WeatherPollClass);
		PollQueue.HeapPush(QueueNode(0, WeatherPoll), QueueNodePredicate());
  }

  // TODO: Initialize this permanent Poll
  if (ArenaPollClass) {
    ArenaPoll = (APoll*) GetWorld()->SpawnActor(ArenaPollClass);
  }
}


// Called when the game starts or when spawned
void APollManagementSystem::BeginPlay()
{
	Super::BeginPlay();
	
	// TESTING ONLY
	// AddPlayer(TEXT("augustomorgan"), TEXT("bwgbq4lfws0kw1o5lj3a5f7ubgqysp"), TEXT("Augusto Morgan"), TEXT("augusto"), nullptr);
	// AddPlayer(TEXT("kib3"), TEXT("og42sic7i8ef920zo3x41kq4zijoq4"), TEXT("Kibe"), TEXT("kibe"), nullptr);
	// AddPlayer(TEXT("manostuart"), TEXT("054pwwl2kz38srsterq0bbd08bgsss"), TEXT("Mano Stuart"), TEXT("stuart"), nullptr);
}


// Called every frame
void APollManagementSystem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}


//Add a player to the Poll System
void APollManagementSystem::AddPlayer(FString PlayerNick, FString PlayerAuthToken, FString PlayerName, AProjectCharacter* Player)
{
	static int32 curId;

	FPlayerInfo p;
	p.PlayerNick = PlayerNick;
	p.PlayerAuthToken = PlayerAuthToken;
	p.PlayerName = PlayerName;
	p.Player = Player;
	p.PlayerTarget = Player->PlayerName;
	p.bCanHostPoll = true;

	Players.Add(p.PlayerNick, p);
	Streamers.Add(p.PlayerNick);

	// Todo: Check this initialization process
	AIRCPollManager* ircManager = (AIRCPollManager*) GetWorld()->SpawnActor(AIRCPollManager::StaticClass());

	ircManager->SetPlayer(p.PlayerNick, p.PlayerAuthToken, p.PlayerName, p.PlayerTarget);
	ircManager->TryConnect();
	
	PlayerIRCManagers.Add(p.PlayerNick, ircManager);

	for (auto& man : PlayerIRCManagers)
	{
		man.Value->SetStreams(Streamers);
	}

  if (ItemPollClass) {
    auto ItemPoll = (APoll*) GetWorld()->SpawnActor(ItemPollClass);
    ItemPoll->Init(++curId, Player);
		PollQueue.HeapPush(QueueNode(0, ItemPoll), QueueNodePredicate());
		ircManager->RegisterPoll(ItemPoll, EPollType::PT_Item);
  }

  if (WeaponPollClass) {
    auto WeaponPoll = (APoll*) GetWorld()->SpawnActor(WeaponPollClass);
    WeaponPoll->Init(++curId, Player);
		PollQueue.HeapPush(QueueNode(0, WeaponPoll), QueueNodePredicate());
		ircManager->RegisterPoll(WeaponPoll, EPollType::PT_Weapon);
  }

  if (UsablePollClass) {
    auto UsablePoll = (APoll*) GetWorld()->SpawnActor(UsablePollClass);
    UsablePoll->Init(++curId, Player);
		PollQueue.HeapPush(QueueNode(0, UsablePoll), QueueNodePredicate());
		ircManager->RegisterPoll(UsablePoll, EPollType::PT_Usable);
  }

  if (TrapPollClass) {
    auto TrapPoll = (APoll*) GetWorld()->SpawnActor(TrapPollClass);
    TrapPoll->Init(++curId, Player);
		PollQueue.HeapPush(QueueNode(0, TrapPoll), QueueNodePredicate());
		ircManager->RegisterPoll(TrapPoll, EPollType::PT_Trap);
  }

  if (GenericPollClass) {
		UE_LOG(LogTemp, Warning, TEXT("Creating Generic Poll Class"));
    auto GenericPoll = (APoll*) GetWorld()->SpawnActor(GenericPollClass);
    GenericPoll->Init(++curId, Player);
		PollQueue.HeapPush(QueueNode(0, GenericPoll), QueueNodePredicate());
		ircManager->RegisterPoll(GenericPoll, EPollType::PT_Generic);
  }

  if (DeathPollClass) {
    auto DeathPoll = (APoll*) GetWorld()->SpawnActor(DeathPollClass);
    DeathPoll->Init(++curId, Player);
    DeathPolls.Add(p.PlayerNick, DeathPoll);
		ircManager->RegisterPoll(DeathPoll, EPollType::PT_Death);
  }

  if (WeatherPoll)
		ircManager->RegisterPoll(WeatherPoll, EPollType::PT_Wheather);

	if (ArenaPoll)
		ircManager->RegisterPoll(ArenaPoll, EPollType::PT_Arena);
}


// Starts a poll and the available keywords
void APollManagementSystem::StartPoll(APoll* Poll)
{
	// if (PollQueue.Num() > 0)
	// {
	// 	FString TargetStream = PollQueue[CurrentPoll];
	// 	if (!PlayerNick.Equals("!CURRENT"))
	// 	{
	// 		TargetStream = PlayerNick;
	// 	}

	// 	for (auto& p : PlayerIRCManagers)
	// 	{
			
	// 		if (p.Key.Equals(TargetStream))
	// 		{
	// 			p.Value->StartPoll(PollName, Keywords, RelevanceLevel, bCanMultipleVotes);
	// 		}
	// 		else
	// 		{
	// 			FString m = FString::Printf(TEXT("Poll started in twitch.tv/%s"), *PollQueue[CurrentPoll]);
	// 			p.Value->HasSentMessage(m);
	// 		}
	// 	}
	// }
}


//	Stops a poll
void APollManagementSystem::StopPoll(APoll* Poll)
{
	// if (PollQueue.Num() > 0)
	// {
	// 	FString TargetStream = PollQueue[CurrentPoll];
	// 	if (!PlayerNick.Equals("!CURRENT"))
	// 	{
	// 		TargetStream = PlayerNick;
	// 	}

	// 	PlayerIRCManagers[TargetStream]->StopPoll(PollName);
	// }
}


// Get the poll's preliminary results, sorted from most voted to least voted
TArray<TPair<int32, TPair<FString, FString> > > APollManagementSystem::GetPollResults(APoll* Poll)
{
	// if (PollQueue.Num() > 0)
	// {
	// 	FString TargetStream = PollQueue[CurrentPoll];
	// 	if (!PlayerNick.Equals(TEXT("!CURRENT")))
	// 	{
	// 		TargetStream = PlayerNick;
	// 	}

	// 	TArray<TPair<int32, TPair<FString, FString> > > Results = PlayerIRCManagers[TargetStream]->GetPollResults(PollName);

	// 	if (Results.Num() > 0)
	// 	{
	// 		FString Msg = FString::Printf(TEXT("Winner: %s %s %d"), *Results[0].Value.Key, *Results[0].Value.Value, Results[0].Key);
	// 		PlayerIRCManagers[TargetStream]->HasSentMessage(Msg);
	// 	}
		
	// 	return Results;
	// }
	
	return TArray<TPair<int32, TPair<FString, FString> > >();
}