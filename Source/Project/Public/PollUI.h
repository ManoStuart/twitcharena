// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "PollUI.generated.h"

/**
 * 
 */
UCLASS(meta=(BlueprintSpawnableComponent), Blueprintable)
class PROJECT_API UPollUI : public UObject
{
	GENERATED_BODY()
	
public:
  UPollUI();


	// ----

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Control")
  bool bActive;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Control")
  int32 id;

  UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Control")
  void Init(int32 nId);


  // ----

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
  FColor PlayerColor;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
  UTexture2D* Image;



  // ----  Queue Widgets

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widget")
  TSubclassOf<class UUserWidget> QueueWidgetClass;
 
  // Variable to hold the widget After Creating it.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
  UUserWidget* QueueWidget;



  // ----  Queue Widgets

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widget")
  TSubclassOf<class UUserWidget> ActiveWidgetClass;
 
  // Variable to hold the widget After Creating it.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
  UUserWidget* ActiveWidget;
};
