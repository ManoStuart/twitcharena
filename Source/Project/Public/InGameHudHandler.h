// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Enums.h"
#include "PollUI.h"
#include "InGameHudHandler.generated.h"


/*************************************************
              Event Declarations
**************************************************/

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPollUICreated, UUserWidget*, QueueWidget, UUserWidget*, ActiveWidget);




/**
 * 
 */
UCLASS(meta=(BlueprintSpawnableComponent), Blueprintable)
class PROJECT_API UInGameHudHandler : public UObject
{
	GENERATED_BODY()

public:	
  UInGameHudHandler();

  static UInGameHudHandler *instance;



  // ----

  UFUNCTION(BlueprintCallable, Category = "General")
  void Init(UUserWidget* nHUD);

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
  UUserWidget* HUD;



  // ----

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Poll")
  TArray<UPollUI *> Polls;
	
  UFUNCTION(BlueprintImplementableEvent, Category = "Poll")
  UPollUI* CreatePollUI(int32 id, AProjectCharacter* Owner, EPollType PollType, EPollInteractionType InteractionType);

  UPROPERTY(BlueprintAssignable, Category = "Poll")
  FPollUICreated OnPollCreated;

  void OnCreatePoll(int32 nId, AProjectCharacter* Owner, EPollType PollType, EPollInteractionType InteractionType);
};
