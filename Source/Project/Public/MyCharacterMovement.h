#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "MyCharacterMovement.generated.h"

UCLASS()
class UMyCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

public:

	friend class FSavedMove_ExtendedMyMovement;

	virtual void UpdateFromCompressedFlags(uint8 Flags) override;

	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetMoveDirection(const FVector& MoveDir);

	//////////////////////////////////////////////////////////////////////////
	// Dodge

	///@brief Checks if Launch was performed.
	bool HasPendingLaunch();

	///@brief Checks if character can dodge.
	bool CanDodge();

	///@brief Triggers the dodge action.
	bool DoDodge();

	//////////////////////////////////////////////////////////////////////////
	// Multijump

	///@brief Event triggered at the end of a movement update
	virtual void OnMovementUpdated(float DeltaSeconds, const FVector & OldLocation, const FVector & OldVelocity) override;

	///@brief Override DoJump to trigger the extra jumps.
	virtual bool DoJump(bool bReplayingMoves) override;

	///@return Whether or not the character can currently jump.
	bool CanJump();

	///@brief This is called whenever the character lands on the ground, and will be used to reset the jump counter.
	virtual void ProcessLanded(const FHitResult& Hit, float remainingTime, int32 Iterations) override;

public:
	//////////////////////////////////////////////////////////////////////////
	// Dodge

	UPROPERTY(EditAnywhere, Category = "Dodge")
	float DodgeStrength;

	UPROPERTY(EditAnywhere, Category = "Dodge")
	float GroundDodgeStrengthMultiplier;

	UPROPERTY(EditAnywhere, Category = "Dodge")
	float DodgeCooldown;

	float DodgeCooldownTimer;

	uint8 bWantsToDodge : 1;

	//////////////////////////////////////////////////////////////////////////
	// Multijump

	UPROPERTY(Category = "Multijump", EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Max Multijump Count"))
	int32 MaxJumpCount;

	UPROPERTY(Category = "Multijump", BlueprintReadWrite, meta = (DisplayName = "Current jump count"))
	int32 JumpCount;

	//////////////////////////////////////////////////////////////////////////
	// General

	FVector MoveDirection;
	bool bDisableAllMovement;
};

class FSavedMove_MyMovement : public FSavedMove_Character
{
public:

	typedef FSavedMove_Character Super;

	///@brief Resets all saved variables.
	virtual void Clear() override;

	///@brief Store input commands in the compressed flags.
	virtual uint8 GetCompressedFlags() const override;

	///@brief This is used to check whether or not two moves can be combined into one.
	///Basically you just check to make sure that the saved variables are the same.
	virtual bool CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const override;

	///@brief Sets up the move before sending it to the server. 
	virtual void SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData) override;

	///@brief This is used to copy state from the saved move to the character movement component.
	///This is ONLY used for predictive corrections, the actual data must be sent through RPC.
	virtual void PrepMoveFor(class ACharacter* Character) override;

	int32 SavedJumpCount;
	FVector SavedMoveDirection;
	uint8 bSavedWantsToDodge : 1;
	float SavedDodgeCooldownTimer;
};

class FNetworkPredictionData_Client_MyMovement : public FNetworkPredictionData_Client_Character
{
public:
	FNetworkPredictionData_Client_MyMovement(const UCharacterMovementComponent& ClientMovement);

	typedef FNetworkPredictionData_Client_Character Super;

	///@brief Allocates a new copy of our custom saved move
	virtual FSavedMovePtr AllocateNewMove() override;
};