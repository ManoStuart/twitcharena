// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "ItemPhysical.h"
#include "DataUtils.generated.h"

UCLASS(Blueprintable, BlueprintType)
class PROJECT_API UDataUtils : public UObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UDataUtils();

	UFUNCTION(BlueprintImplementableEvent, Category = "Physical Item Creation")
	UClass* PhysicalItemBPClass();
};
