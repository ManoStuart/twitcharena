// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Enums.h"
#include "ProjectCharacter.h"
#include "TeamsManager.generated.h"



USTRUCT()
struct FTeamInfo
{
  GENERATED_USTRUCT_BODY();

public:
  // UPROPERTY()
  // The player full name
  // FString TeamName;

  UPROPERTY()
  // Team ID
  int32 Id;

  UPROPERTY()
  // Team Score
  int32 Score;

  // Team Color

  UPROPERTY()
  //Set of players
  TArray<FString> Players;
};

/**
 * 
 */
UCLASS()
class PROJECT_API ATeamsManager : public AActor
{
	GENERATED_BODY()


public:
  ATeamsManager();
  
  static ATeamsManager *instance;

public:
  UPROPERTY()
  // Map of players in the match
  TMap<FString, AProjectCharacter *> Players;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
  // Map of players and their IRC managers
  TArray<FTeamInfo> Teams;

  UPROPERTY()
  // Map of player's teams
  TMap<FString, int32> PlayerTeams;
	
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
  // Current Game Mode
  // TODO: Create states for each gameMode
  EGameMode GameMode;

public:
  UFUNCTION(BlueprintCallable, Category = "Team")
  // Assign a team acording to the current game mode
  int32 AssignTeam(AProjectCharacter* player);

  UFUNCTION(BlueprintCallable, Category = "Team")
  // Get all players from team
  TArray<FString> GetAllPlayersFromTeam(int32 teamId);

protected:

  // Creates a new team
  int32 CreateTeam();

  // Assign a team in a free for all mode
  int32 AssignTeam_ModeFFA(AProjectCharacter* player);
};
