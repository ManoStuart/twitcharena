// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Item.h"
#include "ItemPhysical.generated.h"

//~~~~~ Forward Declarations~~~~~
class AItem;

UCLASS(Blueprintable, BlueprintType)
class PROJECT_API AItemPhysical : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemPhysical();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetItem(UClass* itemClass, bool hidden);
	void SetItem(AItem* item, bool hidden);

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void SetHighlight(bool highlight);

	///////////////////////////////////////////////////////////
	// Static Functions

	static AItemPhysical* Create(AItem* item, AActor* actor, bool hidden = true, float radius = 0);

	UFUNCTION(BlueprintPure, Category = "Physical Item Creation")
	static AItemPhysical* Create(AItem* item, FVector location, UWorld* world, bool hidden = true, float radius = 0);
	static AItemPhysical* Create(UClass* itemClass, AActor* actor, bool hidden = true, float radius = 0);
	static AItemPhysical* Create(UClass* itemClass, FVector location, UWorld* world, bool hidden = true, float radius = 0);


public:
	UPROPERTY(BlueprintReadWrite)
	UMeshComponent* Mesh;

	UPROPERTY(Replicated, BlueprintReadWrite)
	AItem* item;

	bool dirty;
};