// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "DataUtils.h"
#include "Enums.h"
#include "DataSingleton.generated.h"


UCLASS()
class PROJECT_API UDataSingleton : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "Data Singleton")
	static UDataUtils* GetInstance();

	///////////////////////////////////////////////////////////
	// ITEM ENUMS

	UFUNCTION(BlueprintPure, Category = "Item Enums")
	static bool IsChangeItem(EActionQueue action);

	UFUNCTION(BlueprintPure, Category = "Item Enums")
	static bool IsMoveAction(EActionQueue action);

	UFUNCTION(BlueprintPure, Category = "Item Enums")
	static EItemType ItemActionToType(EActionQueue action);

	///////////////////////////////////////////////////////////
	// VECTORS UTILS

	UFUNCTION(BlueprintPure, Category = "Vector Utils")
	static FVector GetCloserGround(UWorld* world, FVector location);

	UFUNCTION(BlueprintPure, Category = "Vector Utils")
	static FVector Random2DRadius(FVector location, float radius);

	UFUNCTION(BlueprintPure, Category = "Vector Utils")
	static FVector RandomGroundRadius(UWorld* world, FVector location, float radius);
};
