// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Enums.h"
#include "Projectile.h"
#include "ProjectCharacter.h"
#include "UnrealNetwork.h"
#include "Item.generated.h"

//~~~~~ Forward Declarations~~~~~
class AProjectCharacter;

USTRUCT(BlueprintType, Blueprintable)
struct FComboNode
{
	GENERATED_USTRUCT_BODY()

	// The next section to play afer a basic action input, -1 equals stop
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ComboNode")
	int32 basic;

	// The next section to play afer a power action input, -1 equals stop
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ComboNode")
	int32 power;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation")
	UAnimSequenceBase* anim;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation")
	USoundBase* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = "Range")
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Melee")
	USoundBase* HitSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Melee")
	float Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
	int32 Usage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
	bool CanMove;

	FComboNode()
	{
		basic = -1;
		power = -1;
		Usage = 1;
		CanMove = true;
		Damage = 0;
	}
};

UCLASS(meta=(BlueprintSpawnableComponent), Blueprintable)
class PROJECT_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
	EItemName ItemName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
	EItemType ItemType;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = "General")
	int32 UsageMax;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attach")
	FName Socket;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attach")
	UAnimSequenceBase* EquipAnim;

	UPROPERTY(BlueprintReadWrite, Category = "Attach")
	UMeshComponent* Mesh;

	// Setup the item's animation montage transitions
	// Each element corresponds to a Section in the montage (Which sould be named ActionX)
	UPROPERTY(EditDefaultsOnly, Category = "Action")
	TArray<FComboNode> ComboSequence;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Material Animation")
	UMaterialInterface* BinaryMaterial;

	UPROPERTY(BlueprintReadWrite, Category = "Material Animation")
	TArray<UMaterialInterface*> SavedMaterials;

	UPROPERTY()
	int32 CurrentCombo;

	UPROPERTY()
	TArray<AProjectCharacter*> Attacked;

public:
	int32 GetNextAnim(int32 num, EActionQueue action);

	UAnimSequenceBase* GetAnimAsset(int32 num, bool& canMove);

	UFUNCTION(BlueprintCallable, Category = "Attach")
	void Equip();

	UFUNCTION(BlueprintCallable, Category = "Attach")
	void Unequip();

	UFUNCTION(BlueprintCallable, Category = "Attach")
	bool ShouldDestroy();

	UFUNCTION(BlueprintImplementableEvent, Category = "Attach")
	void BinaryAnimation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Action")
	bool StartAction(AProjectCharacter* character, FRotator rotation);
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Action")
	void FinishAction();

	UFUNCTION(BlueprintCallable, Category = "Action")
	void OnHit(AActor* damaged);
};