#pragma once

#include "GameFramework/Actor.h"
#include "ProjectCharacter.h"
#include "Projectile.generated.h"

//~~~~~ Forward Declarations~~~~~
class AProjectCharacter;

UCLASS()
class AProjectile : public AActor
{
	GENERATED_BODY()

public:
	AProjectile();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Projectile")
	void SetUp(AProjectCharacter* character);

public:
	UPROPERTY(BlueprintReadWrite, Category = "Projectile")
	int32 Team;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	class USoundBase* HitSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	float Damage;

	UPROPERTY(BlueprintReadWrite, Category = "Projectile")
	AProjectCharacter* Attacker;

	UFUNCTION(BlueprintCallable, Category = "Projectile")
	void OnHit(AActor* damaged, bool friendlyFire = false, bool selfFire = false, bool destroy = true);
};
