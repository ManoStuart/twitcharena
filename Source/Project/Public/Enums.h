// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedEnum.h"
#include "Enums.generated.h"

UENUM(BlueprintType)
enum class EPollKeywordSelection : uint8
{
	PKS_Full  	UMETA(DisplayName = "Full"),
	PKS_Random	UMETA(DisplayName = "Random"),
	PKS_Fixed		UMETA(DisplayName = "Fixed"),
	PKS_Cycle		UMETA(DisplayName = "Cycle"),
};

UENUM(BlueprintType)
enum class EPollTargetSelection : uint8
{
	PTS_Self  			UMETA(DisplayName = "Self"),
	PTS_AllRandom		UMETA(DisplayName = "AllRandom"),
	PTS_TeamRandom	UMETA(DisplayName = "TeamRandom"),
	PTS_All					UMETA(DisplayName = "All"),
	PTS_Team				UMETA(DisplayName = "Team"),
	PTS_None				UMETA(DisplayName = "None"),
};


UENUM(BlueprintType)
enum class EPollType : uint8
{
	PT_Item		  UMETA(DisplayName = "Item"),
	PT_Weapon		UMETA(DisplayName = "Weapon"),
	PT_Usable		UMETA(DisplayName = "Usable"),
	PT_Trap			UMETA(DisplayName = "Trap"),
	PT_Generic	UMETA(DisplayName = "Generic"),
	PT_Wheather	UMETA(DisplayName = "Wheather"),
	PT_Arena		UMETA(DisplayName = "Arena"),
	PT_Death		UMETA(DisplayName = "Death"),
};

UENUM(BlueprintType)
enum class EPollInteractionType : uint8
{
	PIT_Global  UMETA(DisplayName = "Global"),
	PIT_Team		UMETA(DisplayName = "Team"),
	PIT_Player	UMETA(DisplayName = "Player"),
};

UENUM(BlueprintType)
enum class EGameMode : uint8
{
	GM_FFA		  UMETA(DisplayName = "Free for All"),
	GM_TEAM			UMETA(DisplayName = "Teams"),
	GM_MAZE	   	UMETA(DisplayName = "Maze"),
};

UENUM(BlueprintType)
enum class ETeamName : uint8
{
	AQ_None			UMETA(DisplayName = "None"),
	AQ_Blue 		UMETA(DisplayName = "Blue Hawks"),
	AQ_Red			UMETA(DisplayName = "Red Monkeys"),
	AQ_Green		UMETA(DisplayName = "Green Leopards"),
};

UENUM(BlueprintType)
enum class EActionQueue : uint8
{
	AQ_None			UMETA(DisplayName = "None"),
	AQ_Basic 		UMETA(DisplayName = "Basic"),
	AQ_Power		UMETA(DisplayName = "Power"),
	AQ_Released		UMETA(DisplayName = "Released"),
	AQ_Item1		UMETA(DisplayName = "SwapItem_Default"),
	AQ_Item2		UMETA(DisplayName = "SwapItem_Weapon"),
	AQ_Item3		UMETA(DisplayName = "SwapItem_Usable")
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	IT_None			UMETA(DisplayName = "None"),
	IT_Default		UMETA(DisplayName = "Default"),
	IT_Weapon		UMETA(DisplayName = "Weapon"),
	IT_Usable		UMETA(DisplayName = "Usable")
};

UENUM(BlueprintType)
enum class EItemName : uint8
{
	IN_Default		UMETA(DisplayName = "Claw"),
	IN_Mine			UMETA(DisplayName = "Mine"),
	IN_Wand			UMETA(DisplayName = "Wand"),
};