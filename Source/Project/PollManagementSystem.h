// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "IRCPollManager.h"
#include "PollManagementSystem.generated.h"

USTRUCT()
struct FPlayerInfo
{
	GENERATED_USTRUCT_BODY();

public:

	UPROPERTY()
	// The player full name
	FString PlayerName;

	UPROPERTY()
	// The player twitch username 
	FString PlayerNick;

	UPROPERTY()
	// Authentication Token obtained through the Twitch API
	FString PlayerAuthToken;

	UPROPERTY()
	// The String used to represent this player in the polls
	FString PlayerTarget;

	UPROPERTY()
	// The Pawn of this player
	const AProjectCharacter *Player;

	UPROPERTY()
	// Status that shows if the player can host a poll
	// e.g. Is streamming, has been blocked by some ingame mechanics (deicide) etc
	bool bCanHostPoll;
};


struct QueueNode
{
	explicit QueueNode(int32 nPriority, APoll* nPoll) : Priority(nPriority), Poll(nPoll) {}

	int32 Priority;

	APoll* Poll;
};


struct QueueNodePredicate
{
	bool operator()(const QueueNode& A, const QueueNode& B) const
	{
		if (A.Priority == B.Priority) {
			if (A.Poll->Priority < B.Poll->Priority)
				return A.Poll->Id < B.Poll->Id;

			return A.Poll->Priority < B.Poll->Priority;
		}

		return A.Priority < B.Priority;
	}
};
 
 
 // MyType Next;
 // Queue.HeapPop(Next, MyTypePredicate());
 // check(Next.Priority == 20);


UCLASS(meta=(BlueprintSpawnableComponent), Blueprintable)
/*
*	Manage the queue of polls, and all the IRCPollManagers.
*/
class PROJECT_API APollManagementSystem : public AActor
{
	GENERATED_BODY()



  // ----  		QUEUE

public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Queue")
	// Time to wait between each poll. (in)
	float pollDelayTime;

protected:
	//Queue of players to identify polls;
	TArray<QueueNode> PollQueue;

	//Current poll
	APoll* CurrentPoll;

	float currentPollDelay;




  // ----     Poll creation

protected:
	// Pointer to unique Wheather Poll
	APoll * WeatherPoll;

	// Pointer to unique Arena Poll
	APoll * ArenaPoll;

	// Pointer to the Death Poll of each player
	TMap<FString, APoll *> DeathPolls;



  // ----     Poll creation

public:
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> ItemPollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> WeaponPollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> UsablePollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> TrapPollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> GenericPollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> WeatherPollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> ArenaPollClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PollType")
  TSubclassOf<class APoll> DeathPollClass;



  // ----  		IRC Management

public:
	/*
	*	Add a player to the Poll System
	*	@param PlayerNick - the twitch username of the player
	*	@param PlayerAuthToken - the authentication token obtained through twitch API
	*	@param PlayerName - the full name of the player
	*	@param Player - the pawn associated with this player
	*/
	void AddPlayer(FString PlayerNick, FString PlayerAuthToken, FString PlayerName, AProjectCharacter *Player);

protected:
	//Set of streamers
	TArray<FString> Streamers;

	//Map of players in the match
	TMap<FString, FPlayerInfo> Players;

	//Map of players and their IRC managers
	TMap<FString, AIRCPollManager*> PlayerIRCManagers;




  // ----  		Debug

	float Time;
	bool bPoll;




  // ----  		Poll Management

public:	
	/*
	*	Starts a poll, making the necessary network calls, and variables atributions
	*/
	void StartPoll(APoll* Poll);

	/*
	*	Stops the poll
	*/
	void StopPoll(APoll* Poll);

	/*
	*	Get the poll's preliminary results, sorted from most voted to least voted
	*	@return an array of votes and pairs of keywords and targets, sorted by number 
				of votes in descending order
	*/
	TArray<TPair<int32, TPair<FString, FString> > > GetPollResults(APoll* Poll);




  // ----  		Default

public:
  static APollManagementSystem *instance;

	// Sets default values for this actor's properties
	APollManagementSystem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
};
