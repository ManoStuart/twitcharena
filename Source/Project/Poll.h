// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Enums.h"
#include "InGameHudHandler.h"
#include "ProjectCharacter.h"
#include "Poll.generated.h"

/**
 * 
 */
UCLASS(meta=(BlueprintSpawnableComponent), Blueprintable)
class PROJECT_API APoll : public AActor
{
	GENERATED_BODY()


public:
	APoll();



  // ----			GENERAL

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
	EPollType PollType;


  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
	EPollInteractionType PollInteractionType;

	int32 Id;

protected:
	AProjectCharacter* Owner;



  // ----  		QUEUE

public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Queue")
  // Defines how long should the manager wait to restart this poll
	int32 RescheduleTime;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Queue")
  // Who should get priority in case 2 polls want to be started.
	int32 Priority;

protected:
  UPROPERTY(BlueprintReadWrite, Category = "Queue")
  // Defines how long should the manager wait to restart this poll
	int32 CurrentRescheduleTime;




  // ----			KEYWORDS

public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Keywords")
  // Defines how the AvailableKeywords are transformed into the PossibleKeywords
	EPollKeywordSelection KeywordSelection;

	// TODO: how to map a keyword to its resolution (in case it wins the poll)
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Keywords")
  // Contains all keywords that can be used by this kind of poll
	TArray<FString> AvailableKeywords;

	/*
	*	Gets the available keywords for this poll
	*	@return the array of keywords
	*/
	TArray<FString> GetKeywords();

protected:
	// TODO: change to TMAP, so we wont have to reinstantiate at every repoll
	// The valid keywords for the undergoing poll
	TSet<FString> PossibleKeywords;

	/*
	*	Checks the message if the sender has chosen a keyword, updating the voter, keyword and target
	*	@param Message - The received IRC-like message
	*	@param OutVoter - The voter/sender of the message is stored here
	*	@param OutKeyword - The voted keyword (if there is one) is stored here
	*	@param OutTarget - The target of the keyword is stored here (if none, OutTarget = UserTarget)
	*	@return if the message contained a vote
	*/
	bool HasKeyword(FString Message, FString &OutVoter, FString &OutKeyword, FString &OutTarget);




  // ----			TARGETS

public:
  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Targets")
  // Defines how the all the Players of the game are transformed into the PossibleTargets
	EPollTargetSelection TargetSelection;

	/*
	*	Gets the available targets for this poll
	*	@return the array of targets
	*/
	TArray<FString> GetTargets();

protected:
	// The target name of this player, used during polls
	FString DefaultTarget;

	// The valid targets for the undergoing poll
	TSet<FString> PossibleTargets;

	bool bNeedsTarget;

	FString getTargetableKeyword (AProjectCharacter * Target);

	void generatePossibleTargets ();



  // ----			CONTROL

public:
	UFUNCTION(reliable, NetMulticast, withvalidation, Category = "Control")
	//	Sets default values for this actor's properties
	void Init(int32 nId, AProjectCharacter* nOwner);

	/*
	*	Starts a poll by reseting the configurations
	*	@param Keywords - The set of available keywords for this poll
	*	@param Targets - The set of available targets for this poll
	*	@param DefaultTarget - The default target, when no other target is passed
	*/
	void StartPoll(TSet<FString> Keywords, TSet<FString> Targets, FString DefaultTarget);

	/*
	*	Starts a poll without targets by reseting the configurations
	*	@param Keywords - The set of available keywords for this poll
	*/
	void StartPoll(TSet<FString> Keywords);

	/*
	*	Stops the chosen poll and delete its information
	*/
	void StopPoll();




  // ----			VOTES LOGIC

public:

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoteLogic")
	// Are the voters allowed to vote multiple times?
	bool bCanMultipleVotes;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoteLogic")
	// How long does the poll lasts? (in seconds)
	float pollDuration;

	/*
	*	Process the message received looking for the keyword and targets, and registering the vote
	*	@param Message - the IRC message (including IRC command privmsg)
	*/
	void ProcessMessage(FString Message);

	/*
	*	Gets the poll's preliminary results sorted from most votes to less votes
	*	@return an array with the number of votes and the associated pair of keyword/target 
				sorted by number of votes in descending order
	*/
	TArray<TPair<int32, TPair<FString, FString> > > GetPollResults();

protected:
	// The map of all users and their votes pairs - keyword/target - (for unique votes)
	TMap<FString, TPair<FString, FString> > ViewersVotes;

	// The map of all the keywords, and their possible targets pairs and their votes (for multiple votes)
	TMap<FString, TMap<FString, int32> > PairsVotes;

	// Is the poll active?
	bool bIsActive;

	// How long will the poll last from now. (in seconds)
	float currentDuration;


	/*
	*	Register the Voter's vote, with the keyword and target specified
	*	@param Voter - the viewer who sent the vote
	*	@param Keyword - the keyword voted by the voter
	*	@param Target - the target of the voter
	*/
	void ComputeVote(FString Voter, FString Keyword, FString Target);
};
