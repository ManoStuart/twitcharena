// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GameFramework/Character.h"
#include "Item.h"
#include "ItemPhysical.h"
#include "Enums.h"
#include "MyCharacterMovement.h"
#include "Animation/AnimInstance.h"
#include "InGameHudHandler.h"
#include "ProjectCharacter.generated.h"



//~~~~~ Forward Declarations~~~~~
class AItem;




UCLASS(config=Game)
class AProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	AProjectCharacter(const FObjectInitializer& ObjectInitializer);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;



	//////////////////////////////////////////////////////////////////////////
	// Animation Assets

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimationAssets")
	UAnimSequenceBase* HitedAnim;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimationAssets")
	UAnimSequenceBase* DeathAnim;



	//////////////////////////////////////////////////////////////////////////
	// Actions Control

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = "AnimationControl")
	TArray<EActionQueue> ActionQueue;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "AnimationControl")
	int32 CurQueue;

	/// Used for combo validation
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "AnimationControl")
	int32 CurCombo;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "AnimationControl")
	int32 CurSection;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "AnimationControl")
	bool bScheduledUnequip;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "AnimationControl")
	bool InAction;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = "AnimationControl")
	bool bDodging;



	//////////////////////////////////////////////////////////////////////////
	// General

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
	AItem* EquippedItem;

	TMap<EItemType, AItem*> OwnedItems;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "General")
	int32 Team;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
	FString PlayerName;
	
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "General")
	float Health;



	//////////////////////////////////////////////////////////////////////////
	// HUD

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
	UInGameHudHandler* HudHandler;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
  UUserWidget* HUD;




protected:
	//////////////////////////////////////////////////////////////////////////
	// Actions Control

	// Fail Safe for animation lag: -2: unsetted, -1: no animation, X: animation X
	int32 ServerNextSection;

	// Fail Safe for animation lag
	EItemType ServerNextWeapon;

	USkeletalMeshComponent* Mesh;
	UAnimInstance* Animation;



	//////////////////////////////////////////////////////////////////////////
	// Interactions Control

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	TArray<AItemPhysical*> Interactables;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	AItemPhysical* BestInteractable;



	//////////////////////////////////////////////////////////////////////////
	// Movement Control

	UPROPERTY()
	bool DisableAllMovement;



protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;



	//////////////////////////////////////////////////////////////////////////
	// Input Handling

	virtual bool CanJumpInternal_Implementation() const override;

	void MoveForward(float Value);

	void MoveRight(float Value);

	void TurnAtRate(float Rate);

	void LookUpAtRate(float Rate);

	void QueueAction();

	void QueueSubAction();

	void QueueReleasedAction();

	void EquipItem1();

	void EquipItem2();

	void EquipItem3();

	void Interact();

	void Dogde();



	//////////////////////////////////////////////////////////////////////////
	// Animation Utils

	void StopActionCombo();

	bool PlayAnimation(int32 target);

	bool ChangeEquippedItem(EItemType slot);



	//////////////////////////////////////////////////////////////////////////
	// Animation Networking

	UFUNCTION(reliable, server, withvalidation)
	void ServerQueueAction(EActionQueue action);

	UFUNCTION(reliable, NetMulticast, withvalidation)
	void MulticastStartAction(EActionQueue action);

	UFUNCTION(unreliable, NetMulticast, withvalidation)
	void ValidateComboAction(int32 QueuePos, int32 ComboPos, int32 TargetAnimation, EItemType type);



	//////////////////////////////////////////////////////////////////////////
	// Interaction Networking

	UFUNCTION(reliable, server, withvalidation)
	void ServerPickUpItem(AItemPhysical* physical);

	UFUNCTION(reliable, NetMulticast, withvalidation)
	void MulticastPickUpItem(AItemPhysical* physical);



	//////////////////////////////////////////////////////////////////////////
	// Health

	UFUNCTION(BlueprintCallable, Category = "Health")
	void OnTakeAnyDamage(float damage);



public:
	//////////////////////////////////////////////////////////////////////////
	// Default
	virtual void BeginPlay() override;

	void AfterNetworkInit();

	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }



	//////////////////////////////////////////////////////////////////////////
	// Movement Control

	void SetDisableAllMovement(bool move);



	//////////////////////////////////////////////////////////////////////////
	// Animations



	UFUNCTION(BlueprintCallable, Category = "Animation")
	void NextActionQueued();



	//////////////////////////////////////////////////////////////////////////
	// Equip

	UFUNCTION(BlueprintCallable, Category = "Equip")
	void Equip(AItem* item, bool hidden, bool equip);

	UFUNCTION(BlueprintImplementableEvent, Category = "Equip")
	void AttachItem(AItem* item, bool hidden);

	UFUNCTION(BlueprintCallable, Category = "Equip")
	void Unequip(AItem* current);

	UFUNCTION(BlueprintCallable, Category = "Equip")
	void PeformUnequip(AItem* current);

	UFUNCTION(BlueprintCallable, Category = "Equip")
	void EquipItemType(EItemType type);

	UFUNCTION(BlueprintCallable, Category = "Equip")
	AItem* GetItemAt(EItemType type);

	UFUNCTION(BlueprintCallable, Category = "Equip")
	bool HasItemAt(EItemType type);



	//////////////////////////////////////////////////////////////////////////
	// Interactions

	UFUNCTION(BlueprintCallable, Category = "Interactions")
	void RegisterInteractable(AItemPhysical* item);

	UFUNCTION(BlueprintCallable, Category = "Interactions")
	void UnregisterInteractable(AItemPhysical* item);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactions")
	void UpdateBestInteraction();
};
