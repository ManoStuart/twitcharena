// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Poll.h"
#include "Networking.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Enums.h"
#include "IRCPollManager.generated.h"

UCLASS()
/*
*	Responsible for managing IRC interactions for ONE stream and perform polls in this stream
*/
class PROJECT_API AIRCPollManager : public AActor
{
	GENERATED_BODY()

private:
	//----------------- Variables --------------------------

	// The TCP Socket used to connect to the IRC chat
	FSocket* Socket;

	// Tells if the TCP connection was stabilished
	bool bIsConnected;

	// The nick/username of the player's twitch account
	FString UserNick;

	// The token for this twitch account, obtained through the Twitch API
	FString AuthToken;

	// The name of the player (name and last name)
	FString UserName;

	// The target name of this player, used during polls
	FString UserTargetName;

	UPROPERTY()
	// The streams in this same match
	TArray<FString> Streams;

	// The time since the last !keywords request
	float TimeElapsedKeywordsRequest;

	// The time since the last !streams request
	float TimeElapsedStreamsRequest;

	UPROPERTY()
	// The current undergoing Polls, by name
	TMap<FString, APoll*> Polls;

	UPROPERTY()
	// All Polls that can be Polled in this irc channel.
	TMap<EPollType, APoll*> AllPolls;

	//----------------- Methods --------------------------

	/*
	*	Try to send the serialized message to twitch IRC with the current account
	*	NOTE: NOT privmsg to a channel, but the TCP message
	*	@param Message - contains the IRC command to be sent
	*	@return if the message was sent
	*/
	bool HasSentTCPMessage(FString Message);

	/*
	*	Checks if the received message is a ping
	*	@param Message - The received IRC message
	*/
	bool IsPingMessage(FString Message);

	/*
	*	Checks if the received message contains a !keywords request
	*	@param Message - The received IRC message
	*/
	bool IsKeywordsRequest(FString Message);

	/*
	*	Checks if the received message is contains a !streams request
	*	@param Message - The received IRC message
	*/
	bool IsStreamsRequest(FString Message);

	/*
	*	Answer the request for the valid keywords, in the IRC chat
	*/
	void AnswerKeywordsRequest();

	/*
	*	Answer the request for the valid streamers, in the IRC chat
	*/
	void AnswerStreamsRequest();

	//----------------- Debug --------------------------

	// Test variable - to check the poll system
	//float Time;

public:
	/*
	*	Sets default values for this actor's properties
	*/
	AIRCPollManager();

	/*
	*	Sets default values for this actor's properties
	*	@param UserNick - the player's twitch username
	*	@param AuthToken - the authentication token obtained through Twitch API
	*	@param UserName - the complete name of the player
	*	@param UserTargetName - the target name of the player valid during polls
	*/
	AIRCPollManager(FString UserNick, FString AuthToken, FString UserName, FString UserTargetName);

	/*
	*	Called when the game starts or when spawned
	*/
	virtual void BeginPlay() override;

	/*
	*	Called every frame
	*/
	virtual void Tick(float DeltaSeconds) override;

	/*
	*	Sets the player information
	*	@param UserNick - the player's twitch username
	*	@param AuthToken - the authentication token obtained through Twitch API
	*	@param UserName - the complete name of the player
	*	@param UserTargetName - the target name of the player valid during polls
	*/
	void SetPlayer(FString UserNick, FString AuthToken, FString UserName, FString UserTargetName);

	/*
	*	Try to connect to the player's twitch irc channel
	*	@return if the actor succeeded to connect to the IRC channel
	*/
	bool TryConnect();

	/*
	*	Try to send the message to twitch IRC with the current account to the current channel
	*	@param Message - the message to be sent (without IRC commands)
	*	@return if the message was successfully sent
	*/
	bool HasSentMessage(FString Message);

	/*
	*	Start to register votes for a new poll, with the available keywords and targets
	*	@param PollName - the name of the new poll
	*	@param AvailableKeywords - the set of keywords to be considered in this poll
	*	@param AvailableTargets - the set of possible targets
	*	@param RelevanceLevel - the relevance level of the new poll;
	*	@param bCanMultipleVotes - if each voter can vote more then once per poll
	*/
	void StartPoll(FString PollName, TSet<FString> AvailableKeywords, TSet<FString> AvailableTargets, 
		int32 RelevanceLevel = 0, bool bCanMultipleVotes = false);

	/*
	*	Start to register votes for a new poll, with the available keywords
	*	@param PollName - the name of the new poll
	*	@param AvailableKeywords - the set of keywords to be considered in this poll
	*	@param RelevanceLevel - the relevance level of the new poll;
	*	@param bCanMultipleVotes - if each voter can vote more then once per poll
	*/
	void StartPoll(FString PollName, TSet<FString> AvailableKeywords,
		int32 RelevanceLevel = 0, bool bCanMultipleVotes = false);

	/*
	*	Gets the poll's preliminary results sorted from more votes to less votes
	*	@param PollName - the name of the required poll
	*	@return an array with the number of votes and the associated pair of keyword/target sorted
				from the most voted to the least voted
	*/
	TArray<TPair<int32, TPair<FString, FString> > > GetPollResults(FString PollName);

	/*
	*	Stops the chosen poll and delete its information
	*	@param PollName - the name of the required poll
	*/
	void StopPoll(FString PollName);

	/*
	*	Set the list of streams of other players in the same match
	*	@param Streams - The array with the usernames of all the players
	*/
	void SetStreams(TArray<FString> Streams);
	
	// Adds a new poll to the available polls this channel can run
	void RegisterPoll(APoll* Poll, EPollType PollType);
};
