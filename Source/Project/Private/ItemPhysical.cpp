// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "UnrealNetwork.h"
#include "DataSingleton.h"
#include "ItemPhysical.h"

// Sets default values
AItemPhysical::AItemPhysical()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bAlwaysRelevant = true;
	dirty = false;
}

// Called when the game starts or when spawned
void AItemPhysical::BeginPlay()
{
	Super::BeginPlay();
}

void AItemPhysical::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AItemPhysical, item);
}

void AItemPhysical::SetItem(UClass* itemClass, bool hidden) {
	if (!HasAuthority()) return;

	AItem* newItem = GetWorld()->SpawnActor<AItem>(itemClass, this->GetActorLocation(), this->GetActorRotation());

	if (newItem == nullptr)
		UE_LOG(LogTemp, Warning, TEXT("SHIT IS MAD!"));

	newItem->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale, NAME_None);
	newItem->SetActorHiddenInGame(hidden);

	this->item = newItem;
}

void AItemPhysical::SetItem(AItem* nItem, bool hidden) {
	if (!HasAuthority()) return;

	nItem->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale, NAME_None);
	nItem->SetActorHiddenInGame(hidden);

	this->item = nItem;
}

// Called every frame
void AItemPhysical::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	// Rotate Item
	FRotator curRot = GetActorRotation();
	curRot.Yaw += DeltaTime * 90;
	SetActorRotation(curRot);
}

void AItemPhysical::SetHighlight(bool highlight)
{
	Mesh->SetRenderCustomDepth(highlight);
}

///////////////////////////////////////////////////////////
// Static Functions

AItemPhysical* AItemPhysical::Create(AItem* item, AActor* actor, bool hidden, float radius)
{
	return AItemPhysical::Create(item, actor->GetActorLocation(), actor->GetWorld(), hidden);
}

AItemPhysical* AItemPhysical::Create(AItem* item, FVector location, UWorld* world, bool hidden, float radius)
{
	UE_LOG(LogTemp, Error, TEXT("Spawning item!"));
	item->DetachRootComponentFromParent();

	location = UDataSingleton::RandomGroundRadius(world, location, radius);

	AItemPhysical* physical = world->SpawnActor<AItemPhysical>(UDataSingleton::GetInstance()->PhysicalItemBPClass(), location, FRotator::ZeroRotator);
	if (physical == nullptr) return nullptr;

	physical->SetItem(item, hidden);

	return physical;
}

AItemPhysical* AItemPhysical::Create(UClass* itemClass, AActor* actor, bool hidden, float radius)
{
	return AItemPhysical::Create(itemClass, actor->GetActorLocation(), actor->GetWorld(), hidden);
}

AItemPhysical* AItemPhysical::Create(UClass* itemClass, FVector location, UWorld* world, bool hidden, float radius)
{
	location = UDataSingleton::RandomGroundRadius(world, location, radius);

	AItemPhysical* physical = world->SpawnActor<AItemPhysical>(UDataSingleton::GetInstance()->PhysicalItemBPClass(), location, FRotator::ZeroRotator);
	if (physical == nullptr) return nullptr;

	physical->SetItem(itemClass, hidden);

	return physical;
}