// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "Blueprint/UserWidget.h"
#include "PollUI.h"




UPollUI::UPollUI()
{
  if (QueueWidgetClass)
    QueueWidget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), QueueWidgetClass);
  
  if (ActiveWidgetClass)
    ActiveWidget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), ActiveWidgetClass);
}


void UPollUI::Init_Implementation(int32 nId)
{
  id = nId;
}