// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "TeamsManager.h"

/* **********************
          UTILS
*********************** */

char* gen_random_str(const int len) {
  static const char alphanum[] =
    "0123456789"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz";

  char* str = new char[len];

  for (int i = 0; i < len; ++i) {
      str[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  }

  str[len] = '\0';

  return str;
}


/* **********************
          CLASS
*********************** */

ATeamsManager* ATeamsManager::instance;

ATeamsManager::ATeamsManager()
{
  // Creating singleton
  instance = this;

  GameMode = EGameMode::GM_FFA;
}

int32 ATeamsManager::AssignTeam(AProjectCharacter* player)
{
  switch(GameMode)
  {
    case EGameMode::GM_FFA:
      return AssignTeam_ModeFFA(player);
    default:
      return AssignTeam_ModeFFA(player);
  }
}

int32 ATeamsManager::CreateTeam()
{
  FTeamInfo team;

  team.Score = 0;
  team.Id    = Teams.Num() + 1;

  // Set team color and name

  Teams.Push(team);

  return team.Id;
}

int32 ATeamsManager::AssignTeam_ModeFFA(AProjectCharacter* player)
{
  player->PlayerName = FString(ANSI_TO_TCHAR(gen_random_str(10)));

  Players.Add(player->PlayerName, player);

  int32 newTeam = CreateTeam();

  player->Team = newTeam;

  PlayerTeams.Add(player->PlayerName, newTeam);

  return newTeam;
}


TArray<FString> ATeamsManager::GetAllPlayersFromTeam(int32 teamId)
{
  auto team = Teams[teamId];

  return team.Players;
}