// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "Item.h"

// Sets default values
AItem::AItem()
{
	bReplicates = true;
	bAlwaysRelevant = true;
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	DisableComponentsSimulatePhysics();
	SetActorEnableCollision(false);
}

void AItem::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AItem, UsageMax);
}

UAnimSequenceBase* AItem::GetAnimAsset(int32 num, bool& canMove)
{
	canMove = true;
	if (num <= -1 || num >= ComboSequence.Num()) return nullptr;
	FComboNode node = ComboSequence[num];

	canMove = node.CanMove;
	return node.anim;
}

int32 AItem::GetNextAnim(int32 num, EActionQueue action)
{
	if (num <= -1 || num >= ComboSequence.Num()) return -1;
	FComboNode node = ComboSequence[num];
	CurrentCombo = -1;

	int32 next;
	if (action == EActionQueue::AQ_Basic)
		next = node.basic;
	else if (action == EActionQueue::AQ_Power)
		next = node.power;
	else
		return -1;

	if (next <= -1 || next >= ComboSequence.Num()) return -1;
	node = ComboSequence[next];

	if (UsageMax - node.Usage < 0) {
		// Play failure sound
		return -1;
	}

	CurrentCombo = next;
	return next;
}


void AItem::Equip()
{
	this->BinaryAnimation();
	this->SetActorHiddenInGame(false);
}

void AItem::Unequip()
{
}

bool AItem::ShouldDestroy()
{
	return UsageMax <= 0;
}

bool AItem::StartAction_Implementation(AProjectCharacter* character, FRotator rotation)
{
	//UE_LOG(LogTemp, Error, TEXT("START ACTION %s!"), *(FString::FromInt(num)));
	if (CurrentCombo <= -1 || CurrentCombo >= ComboSequence.Num()) return false;
	FComboNode current = ComboSequence[CurrentCombo];

	// try and play the sound if specified
	if (current.FireSound != NULL)
		UGameplayStatics::PlaySoundAtLocation(this, current.FireSound, GetActorLocation());

	// Reduce Usage
	if (UsageMax < 9000)
		UsageMax -= current.Usage;

	if (HasAuthority())
	{
		// try and fire a projectile
		if (current.ProjectileClass != NULL)
		{
			UWorld* const World = GetWorld();
			if (World != NULL)
			{
				AProjectile* proj = World->SpawnActor<AProjectile>(current.ProjectileClass, GetActorLocation(), rotation);
				proj->SetUp(character);
			}
		}
	}

	// Enable Collisions
	Attacked.Empty();
	SetActorEnableCollision(true);


	// Destroy item
	if (UsageMax <= 0) return true;
	return false;
}

void AItem::FinishAction_Implementation()
{
	//UE_LOG(LogTemp, Warning, TEXT("Stop Colliding!"));
	SetActorEnableCollision(false);
}

void AItem::OnHit(AActor* damaged)
{
	//UE_LOG(LogTemp, Error, TEXT("Collided!"));
	if (CurrentCombo <= -1 || CurrentCombo >= ComboSequence.Num()) return;
	FComboNode current = ComboSequence[CurrentCombo];

	AActor* parent = GetAttachParentActor();

	if (parent == damaged || parent == nullptr) return;

	auto parentCharacter = Cast<AProjectCharacter>(parent);
	auto damagedCharacter = Cast<AProjectCharacter>(damaged);

	// return on null cast or same team
	if (parentCharacter == nullptr || damagedCharacter == nullptr || parentCharacter->Team == damagedCharacter->Team) return;

	if (Attacked.Find(damagedCharacter) != INDEX_NONE) return;
	Attacked.Add(damagedCharacter);

	// Play Hit Sound
	if (current.HitSound != nullptr)
		UGameplayStatics::PlaySoundAtLocation(this, current.HitSound, GetActorLocation());

	if (HasAuthority())
	{
		FDamageEvent damageEvent;
		damagedCharacter->TakeDamage(current.Damage, damageEvent, parentCharacter->GetController(), this);
	}
}