
#include "Project.h"
#include "MyCharacterMovement.h"

UMyCharacterMovement::UMyCharacterMovement(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

void UMyCharacterMovement::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);

	//The Flags parameter contains the compressed input flags that are stored in the saved move.
	//UpdateFromCompressed flags simply copies the flags from the saved move into the movement component.
	//It basically just resets the movement component to the state when the move was made so it can simulate from there.
	bWantsToDodge = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
}

bool UMyCharacterMovement::ServerSetMoveDirection_Validate(const FVector& MoveDir)
{
	return true;
}

void UMyCharacterMovement::ServerSetMoveDirection_Implementation(const FVector& MoveDir)
{
	MoveDirection = MoveDir;
}

void UMyCharacterMovement::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);

	if (!CharacterOwner)
	{
		return;
	}

	//Store movement vector
	if (PawnOwner->IsLocallyControlled())
	{
		MoveDirection = PawnOwner->GetLastMovementInputVector();

		//Send movement vector to server
		if (PawnOwner->Role < ROLE_Authority)
		{
			ServerSetMoveDirection(MoveDirection);
		}
	}

	//Update dodge movement
	if (bWantsToDodge && CanDodge())
	{
		MoveDirection.Normalize();
		FVector DodgeVel = MoveDirection*DodgeStrength;
		DodgeVel.Z = 0.0f;

		if (IsMovingOnGround())
		{
			DodgeVel *= GroundDodgeStrengthMultiplier;
		}

		Launch(DodgeVel);

		bWantsToDodge = false;

		//Reset cooldown timer
		DodgeCooldownTimer = DodgeCooldown;
	}

	//Update cooldown timers
	if (DodgeCooldownTimer > 0.0f)
		DodgeCooldownTimer -= DeltaSeconds;
}

//////////////////////////////////////////////////////////////////////////
// Dodge

bool UMyCharacterMovement::HasPendingLaunch()
{
	return !PendingLaunchVelocity.IsZero() && HasValidData();
}

bool UMyCharacterMovement::CanDodge()
{
	return DodgeCooldownTimer <= 0.0f && !bDisableAllMovement;
}

bool UMyCharacterMovement::DoDodge()
{
	if (CanDodge()) {
		bWantsToDodge = true;
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////
// Multijump

bool UMyCharacterMovement::CanJump()
{
	return (IsMovingOnGround() || JumpCount < MaxJumpCount) && CanEverJump() && !bDisableAllMovement;
}

bool UMyCharacterMovement::DoJump(bool bReplayingMoves)
{
	if (Super::DoJump(bReplayingMoves))
	{
		JumpCount++;

		//Adjust midair velocity using the input direction
		if (JumpCount > 1)
		{
			//Calculate lateral speed to use in adjusting trajectory in midair
			FVector LateralVelocity = Velocity;
			LateralVelocity.Z = 0.0f;//Don't care about vertical velocity
			float LateralSpeed = LateralVelocity.Size();

			//Average the actual velocity with the target velocity
			FVector NewVelocity = MoveDirection*LateralSpeed;
			NewVelocity.Z = 0.0f;
			NewVelocity += LateralVelocity;
			NewVelocity *= 0.5f;

			Velocity = NewVelocity;
			Velocity.Z = JumpZVelocity;
		}

		return true;
	}

	return false;
}

void UMyCharacterMovement::ProcessLanded(const FHitResult& Hit, float remainingTime, int32 Iterations)
{
	JumpCount = 0;

	Super::ProcessLanded(Hit, remainingTime, Iterations);
}

//////////////////////////////////////////////////////////////////////////
// Default

class FNetworkPredictionData_Client* UMyCharacterMovement::GetPredictionData_Client() const
{
	check(CharacterOwner != NULL);
	checkSlow(CharacterOwner->Role < ROLE_Authority || (CharacterOwner->GetRemoteRole() == ROLE_AutonomousProxy && GetNetMode() == NM_ListenServer));
	checkSlow(GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer);

	if (!ClientPredictionData)
	{
		UMyCharacterMovement* MutableThis = const_cast<UMyCharacterMovement*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_MyMovement(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

uint8 FSavedMove_MyMovement::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();

	if (bSavedWantsToDodge)
	{
		Result |= FLAG_Custom_0;
	}

	return Result;
}

void FSavedMove_MyMovement::Clear()
{
	Super::Clear();

	//Clear variables back to their default values.
	SavedJumpCount = 0;
	SavedMoveDirection = FVector::ZeroVector;
	bSavedWantsToDodge = false;
	SavedDodgeCooldownTimer = 0;
}

bool FSavedMove_MyMovement::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	//This pretty much just tells the engine if it can optimize by combining saved moves. There doesn't appear to be
	//any problem with leaving it out, but it seems that it's good practice to implement this anyways.

	if (SavedJumpCount != ((FSavedMove_MyMovement*)&NewMove)->SavedJumpCount)
		return false;

	if (SavedMoveDirection != ((FSavedMove_MyMovement*)&NewMove)->SavedMoveDirection)
		return false;

	if (bSavedWantsToDodge != ((FSavedMove_MyMovement*)&NewMove)->bSavedWantsToDodge)
		return false;

	if (SavedDodgeCooldownTimer != ((FSavedMove_MyMovement*)&NewMove)->SavedDodgeCooldownTimer)
		return false;

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

void FSavedMove_MyMovement::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UMyCharacterMovement* CharMov = Cast<UMyCharacterMovement>(Character->GetCharacterMovement());
	if (CharMov)
	{
		//This is just the exact opposite of SetMoveFor. It copies the state from the saved move to the movement
		//component before a correction is made to a client.
		CharMov->JumpCount = SavedJumpCount;

		CharMov->MoveDirection = SavedMoveDirection;

		CharMov->bWantsToDodge = bSavedWantsToDodge;
		
		CharMov->DodgeCooldownTimer = SavedDodgeCooldownTimer;

		//Don't update flags here. They're automatically setup before corrections using the compressed flag methods.
	}
}

void FSavedMove_MyMovement::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character& ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UMyCharacterMovement* CharMov = Cast<UMyCharacterMovement>(Character->GetCharacterMovement());
	if (CharMov)
	{
		//This is literally just the exact opposite of UpdateFromCompressed flags. We're taking the input
		//from the player and storing it in the saved move.
		bSavedWantsToDodge = CharMov->bWantsToDodge;

		SavedDodgeCooldownTimer = CharMov->DodgeCooldownTimer;

		//Again, just taking the player movement component's state and storing it for later it in the saved move.
		SavedMoveDirection = CharMov->MoveDirection;

		SavedJumpCount = CharMov->JumpCount;
	}
}

FNetworkPredictionData_Client_MyMovement::FNetworkPredictionData_Client_MyMovement(const UCharacterMovementComponent& ClientMovement)
	: FNetworkPredictionData_Client_Character(ClientMovement)
{}

FSavedMovePtr FNetworkPredictionData_Client_MyMovement::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_MyMovement());
}