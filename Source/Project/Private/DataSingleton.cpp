// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "DataSingleton.h"

UDataUtils* UDataSingleton::GetInstance()
{
	UDataUtils* DataInstance = Cast<UDataUtils>(GEngine->GameSingleton);

	if (!DataInstance) return NULL;
	if (!DataInstance->IsValidLowLevel()) return NULL;

	return DataInstance;
}

///////////////////////////////////////////////////////////
// ITEM ENUMS

bool UDataSingleton::IsChangeItem(EActionQueue action)
{
	return action == EActionQueue::AQ_Item1 || action == EActionQueue::AQ_Item2 || action == EActionQueue::AQ_Item3;
}

bool UDataSingleton::IsMoveAction(EActionQueue action)
{
	return action == EActionQueue::AQ_Basic || action == EActionQueue::AQ_Power;
}

EItemType UDataSingleton::ItemActionToType(EActionQueue action) {
	if (action == EActionQueue::AQ_Item1)
		return EItemType::IT_Default;
	if (action == EActionQueue::AQ_Item2)
		return EItemType::IT_Weapon;
	if (action == EActionQueue::AQ_Item3)
		return EItemType::IT_Usable;

	return EItemType::IT_None;
}


///////////////////////////////////////////////////////////
// VECTORS UTILS

FVector UDataSingleton::GetCloserGround(UWorld* world, FVector location)
{
	FVector endUp = location + FVector(0.f, 0.f, 4000.f);
	FVector endDown = location + FVector(0.f, 0.f, -4000.f);

	FHitResult HitResultUp;
	bool hitUp =
		world->LineTraceSingleByChannel(HitResultUp, location, endUp, ECollisionChannel::ECC_GameTraceChannel1);

	FHitResult HitResultDown;
	bool hitDown =
	world->LineTraceSingleByChannel(HitResultDown, location, endDown, ECollisionChannel::ECC_GameTraceChannel1);

	if (!hitDown) {
		if (!hitUp) return location;
	}
	else if (!hitUp || HitResultUp.Distance > HitResultDown.Distance)
		HitResultUp = HitResultDown;

	return HitResultUp.ImpactPoint;
}

FVector UDataSingleton::Random2DRadius(FVector location, float radius)
{
	if (radius <= 0) return location;

	FVector rand = FMath::VRand();
	rand.Z = 0;
	rand.Normalize();

	return location + rand * radius;
}

FVector UDataSingleton::RandomGroundRadius(UWorld* world, FVector location, float radius)
{
	return UDataSingleton::GetCloserGround(world, UDataSingleton::Random2DRadius(location, radius));
}