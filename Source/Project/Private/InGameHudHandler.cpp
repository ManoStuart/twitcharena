// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "InGameHudHandler.h"



UInGameHudHandler* UInGameHudHandler::instance;

UInGameHudHandler::UInGameHudHandler()
{
  // Creating singleton
  instance = this;
}


void UInGameHudHandler::Init(UUserWidget* nHUD)
{
  HUD = nHUD;
  UE_LOG(LogTemp, Warning, TEXT("UInGameHudHandler Init"));
}


void UInGameHudHandler::OnCreatePoll(int32 nId, AProjectCharacter* Owner, EPollType PollType, EPollInteractionType InteractionType)
{
  UE_LOG(LogTemp, Error, TEXT("UInGameHudHandler OnCreatePoll"));
  auto nPoll = CreatePollUI(nId, Owner, PollType, InteractionType);

  Polls.Push(nPoll);

  OnPollCreated.Broadcast(nPoll->QueueWidget, nPoll->ActiveWidget);
}