// Fill out your copyright notice in the Description page of Project Settings.

#include "Project.h"
#include "Projectile.h"

AProjectile::AProjectile()
{
	bReplicates = true;
}

void AProjectile::SetUp_Implementation(AProjectCharacter* character)
{
	Team = character->Team;
	Attacker = character;
}

void AProjectile::OnHit(AActor* damaged, bool friendlyFire, bool selfFire, bool destroy)
{
	//UE_LOG(LogTemp, Error, TEXT("Collided!"));

	if (Attacker == nullptr) return;

	// Shouldnt hit himself
	if (Attacker == damaged && !selfFire) return;

	// Play Hit Sound
	if (HitSound != nullptr)
		UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());

	if (HasAuthority())
	{
		auto damagedCharacter = Cast<AProjectCharacter>(damaged);

		// return on null cast or same team
		if (damagedCharacter != nullptr && (Attacker->Team != damagedCharacter->Team || friendlyFire)) {
			FDamageEvent damageEvent;
			damagedCharacter->TakeDamage(Damage, damageEvent, Attacker->GetController(), this);
		}

		if (destroy)
			Destroy();
	}
}